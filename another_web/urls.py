"""draft_lab URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
	
	"""

from django.conf.urls import url
from django.urls import include, path
from .views import index
from .views import aboutme
from .views import experience
from .views import skillANDinterest
from .views import contact
from .views import register
from .views import schedule
from .views import sched_result
from .views import delete

urlpatterns = [
	url(r'^$', index, name ='index'),
	url(r'^aboutme/', aboutme, name ='aboutme'),
	url(r'^experience/', experience, name ='experience'),
	url(r'^skillANDinterest/', skillANDinterest, name ='skillANDinterest'),
	url(r'^contact/', contact, name ='contact'),
	url(r'^register/', register, name ='register'),
	url(r'^schedule/', schedule, name='schedule'),
	url(r'^sched_result/', sched_result, name='sched_result'),
	url(r'^delete/', delete, name='delete'),
	
	]