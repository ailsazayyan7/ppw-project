from django import forms
class My_Schedule(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    activity = forms.CharField(label = 'your activity', required = True, max_length=27, widget = forms.TextInput(attrs=attrs))
    date = forms.DateField(label = 'start', required = True, widget = forms.DateInput(attrs={'class':'form-control', 'type':'date'}))
    time = forms.TimeField(label = 'start_time', required = True, widget = forms.TimeInput(attrs={'class':'form-control', 'type':'time'}))
    place= forms.CharField(label = 'place', required = True, widget = forms.TextInput(attrs=attrs))
    category = forms.CharField(label = 'category', required = True, widget = forms.TextInput(attrs=attrs))
