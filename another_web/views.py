from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import Schedule
from .forms import My_Schedule

# Create your views here.

response = {'author' : 'ailsa'}

def index(request):
	return render(request, 'home.html')

def aboutme(request):
	return render(request, 'aboutme.html')
	
def experience(request):  
	return render(request, 'experience.html')
	
def skillANDinterest(request):
	return render(request, 'skillANDinterest.html')
	
def contact(request):
	return render(request, 'contact.html')
	
def register(request):
	return render(request, 'register.html')
	

#lab5!

	
def schedule(request):
	if (request.method == "POST" ):
		form = My_Schedule(request.POST)
		if (form.is_valid()):
			response['activity'] = request.POST['activity']
			response['date'] = request.POST['date']
			response['time'] = request.POST['time']
			response['place'] = request.POST['place']
			response['category'] = request.POST['category']
			sched = Schedule(activity= response['activity'], date=response['date'], time = response['time']
								, place=response['place'],
								category=response['category'])
			sched.save()
			#form.save()
			table = Schedule.objects.all()
		#return render (request, 'form_result', {'table_schedule' : table})
		return HttpResponseRedirect('sched_result/')
	else:
		print('hehe')
	form = My_Schedule()
	html = 'form_page.html'
	return render(request, html, {'sched_form':form})
def sched_result(request):
	table = Schedule.objects.all()
	return render(request, 'form_result.html', {'table_schedule': table})
	
def delete(request):
	Schedule.objects.all().delete()
	return HttpResponseRedirect('form_result.html')

	
